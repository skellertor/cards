import {
  fetchShuffledDeckId,
  drawCards,
  partition,
  order,
  log
} from "./helpers.mjs";

async function main() {
  try {
    const deck = await fetchShuffledDeckId()
    const cards = await drawCards(deck)
    const partitions = partition(cards)
    const ordered = order(partitions)
    log(ordered)
  } catch(err) {
    console.log("\n")
    console.log(err.message)
    console.log("Looks like something went wrong 🤷. Please come back later and we'll find those queens!")
  }
}

main();

// fetchShuffledDeckId()
//   .then(drawCards)
//   .then(partition)
//   .then(order)
//   .then(log)

// (async () => {
//   try {
//     const deck = await fetchShuffledDeckId()
//     const cards = await drawCards(deck)
//     const partitions = partition(cards)
//     const ordered = order(partitions)
//     log(ordered)
//   } catch(err) {
//     console.log("\n")
//     console.log(err.message)
//     console.log("Looks like something went wrong 🤷. Please come back later and we'll find those queens!")
//   }
// })();

// try {
//   const deck = await fetchShuffledDeckId()
//   const cards = await drawCards(deck)
//   const partitions = partition(cards)
//   const ordered = order(partitions)
//   log(ordered)
// } catch(err) {
//   console.log("\n")
//   console.log(err.message)
//   console.log("Looks like something went wrong 🤷. Please come back later and we'll find those queens!")
// }


