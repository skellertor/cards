import axios from "axios"

const ORDER_MAP = {
  "ACE": 1,
  "2": 2,
  "3": 3,
  "4": 4,
  "5": 5,
  "6": 6,
  "7": 7,
  "8": 8,
  "9": 9,
  "10": 10,
  "JACK": 11,
  "QUEEN": 12,
  "KING": 13
}

const EMOJI_MAP = {
  "ACE": "❌",
  "2": "❌",
  "3": "❌",
  "4": "❌",
  "5": "❌",
  "6": "❌",
  "7": "❌",
  "8": "❌",
  "9": "❌",
  "10": "❌",
  "JACK": "❌",
  "QUEEN": "👑",
  "KING": "❌"
}

// number of queens until we have found them all
const ALL_QUEENS = 4
// a one second delay errors out with a 500 more frequently
const DELAY = 2000
const BASE_URL = "https://deckofcardsapi.com"

export async function fetchShuffledDeckId() {
  const response = await axios.get(`${BASE_URL}/api/deck/new/shuffle/`)
  return { deck: response?.data?.deck_id ?? "" }
}

export async function drawCards({ 
  deck,
  howMany = 2,
  queenCount = 0,
  allCards = []
}) {
  if (queenCount < ALL_QUEENS) {
    const result = await new Promise((resolve, reject) => {
      setTimeout(() => {
        return axios.get(`${BASE_URL}/api/deck/${deck}/draw/?count=${howMany}`)
          .then(resolve)
          .catch(reject)
      }, DELAY)
    })
    const cards = result?.data?.cards || []
    const queens = cards.filter((card) => card.value === "QUEEN")
    // visual feedback that it is doing something
    cards.forEach((card) => process.stdout.write(EMOJI_MAP[card.value]))
      
    return await drawCards({
      deck,
      howMany,
      queenCount: queenCount + queens.length,
      allCards: [
        ...allCards,
        ...cards.map((card) => ({ 
          value: card.value, 
          suit: card.suit,
          order: ORDER_MAP[card.value] 
        }))
      ]
    })
  } else {
    return allCards
  }
}

export function partition(cards) {
  return cards.reduce((acc, { suit, value, order }) => {
    if (acc[suit]) {
      acc[suit].push({ value, order })
    } else {
      acc[suit] = [{ value, order }]
    }
    return acc
  }, {})
}

export function order(partitions) {
  return Object.entries(partitions)
    .reduce((acc, [suit, cards]) => {
      acc[suit] = cards.sort((a, b) => {
        return a.order - b.order
      })
        .map((card) => card.value)
      return acc
    }, {})
}

export function log(ordered) {
  console.log("\n")
  const keys = Object.keys(ordered)
  keys.forEach((key) => {
    console.log(`${key}: ${ordered[key]}`)
  })
}