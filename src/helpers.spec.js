import axios from "axios"
import {
  fetchShuffledDeckId,
  partition,
  order
} from "./helpers.mjs"

describe("helpers.mjs", () => {
  describe("fetchShuffledDeckId", () => {
    it("should return a string if api returns `deck_id`", async () => {
      const axiosSpy = spyOn(axios, "get").and.returnValue(Promise.resolve({ data: { deck_id: "id" } }))
      const id = await fetchShuffledDeckId()
      expect(axiosSpy).toHaveBeenCalled()
      expect(id.length).toBe(2)
    })
    it("should return an empty string if api does not return `deck_id`", async () => {
      const axiosSpy = spyOn(axios, "get").and.returnValue(Promise.resolve({}))
      const id = await fetchShuffledDeckId()
      expect(axiosSpy).toHaveBeenCalled()
      expect(id.length).toBe(0)
    }) 
  })
  describe("partition", () => {
    it("should partition single array into multiple", () => {
      const cards = [
        {
          suit: "HEARTS",
          value: "QUEEN",
          order: 12
        },
        {
          suit: "SPADES",
          value: "ACE",
          order: 1
        }
      ]
      const result = partition({ cards })
      expect(result).toEqual(jasmine.objectContaining({
        HEARTS: jasmine.any(Array),
        SPADES: jasmine.any(Array)
      }))
    })
  })
  describe("order", () => {
    it("should order unordered values", () => {
      const partitions = {
        HEARTS: [{ value: "QUEEN", order: 12 }],
        SPADES: [{ value: "KING", order: 13 }, { value: "ACE", order: 1 }, { value: "9", order: 9 }]
      }
      const result = order({ partitions })
      expect(result.SPADES).toEqual(["ACE","9","KING"])
    })
  })
})